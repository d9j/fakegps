# FakeGPS #

[The app page](https://play.google.com/store/apps/details?id=com.al.fakegps.app)

Can be used to set the current fake GPS location using the map 

##Features.
* Map navigation . 
* Search location on the map 
* Keep the history of fake locations that were set 
* Add Favorite locations