package com.al.fakegps.app;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class OnStartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean onBoot = AppSettings.bootStartup(context);
        Log.d("start_gps","Starting gps " + onBoot );
        if(onBoot){
            Intent i = new Intent(context,MyMapService.class);
            context.startService(i);
        }
    }

}
