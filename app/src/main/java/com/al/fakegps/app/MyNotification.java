package com.al.fakegps.app;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
public class MyNotification extends NotificationCompat{
    protected final int notif_id = 20;
    private Context mContext;
    private NotificationManager mNotificationManager;

    public MyNotification(Context context) {
        this.mContext = context;
    }
    public  void buildNotification(String state_name){

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(notif_id);
        Builder mBuilder =
                new Builder(mContext)
                        .setSmallIcon(R.drawable.fake_gps)
                        .setContentTitle(mContext.getString(R.string.fake_location))
                        .setContentText(mContext.getString(R.string.current_location,state_name));
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent  = new Intent(mContext, MapsActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
// mId allows you to update the notification later on.
        mNotificationManager.notify(notif_id, mBuilder.build());
    }
    public void cancel(){
        mNotificationManager.cancel(notif_id);
    }


}