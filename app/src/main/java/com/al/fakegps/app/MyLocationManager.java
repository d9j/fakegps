package com.al.fakegps.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.*;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;


public class MyLocationManager {
    private LocationManager mLocationManager;
    private Context mContext;
    private final String M_FAKE = "LOC_FAKER";
    public MyLocationManager(){}
    public MyLocationManager(Context mContext) {
        this.mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        this.mContext = mContext;
        setProvider();
    }
    private void setProvider(){
        try{
            mLocationManager.addTestProvider(LocationManager.GPS_PROVIDER, false, false, false, false, false, false, false, 1, 1);

        }catch (Exception exc){
            SysUtil.showLog(exc);
            if(exc instanceof  IllegalArgumentException){
                mLocationManager.removeTestProvider(LocationManager.GPS_PROVIDER);
                mLocationManager.addTestProvider(LocationManager.GPS_PROVIDER, false, false, false, false, false, false, false, 1, 1);
            }else if( exc instanceof SecurityException){
                requireTosetMockSetting(mContext);
                return;
            }else {
                Toast.makeText(mContext, mContext.getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
            }
        }
        mLocationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);
    }
    protected void removeProvider() {
        try {
            mLocationManager.removeTestProvider(LocationManager.GPS_PROVIDER);
        }catch (IllegalArgumentException e){
            SysUtil.showLog(e);
        }
    }
    public  void setLocation(LatLng latLng) throws IllegalArgumentException  {
        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        location.setAccuracy(0F);
        location.setTime(System.currentTimeMillis());
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 ){
            SysUtil.showLog("setting_elapsed");
            location.setElapsedRealtimeNanos(SystemClock.elapsedRealtime());
        }
        try{
            mLocationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER,location);
        }catch (Exception e){
            if( e instanceof SecurityException){
                SysUtil.showLog(e);
                requireTosetMockSetting(mContext);
            }else{
                SysUtil.showLog(e);
                Toast.makeText(mContext, mContext.getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
            }

        }
    }
    public static void requireTosetMockSetting(Context mContext){
        Toast.makeText(mContext,mContext.getString(R.string.enable_mock_locations),Toast.LENGTH_LONG).show();
        Intent i = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }



}