package com.al.fakegps.app;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class MyMapService extends Service {

    public final  static  String STATE_RECEIVER = "state_receiver_action";
    public final  static  String STATE_ID = "state_id";
    public final static String DESTROY_SERVICE = "destroy_service";

    MyNotification myNotification;

    MyLocationManager myLocationManager;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       registerReceiver(mLocationCurrentReceiver, new IntentFilter(STATE_RECEIVER));
        boolean showIcon = AppSettings.showIcon(getApplicationContext());
        LocationModel addressModel = AppSettings.getCurrentLocation(getApplication());
        myLocationManager = new MyLocationManager(getApplicationContext());
        myLocationManager.setLocation(addressModel.getLatLng());

        if(showIcon){
            myNotification = new MyNotification(getApplicationContext());
            myNotification.buildNotification(addressModel.getLatLng().toString());
        }



        return super.onStartCommand(intent, flags, startId);
    }
    private BroadcastReceiver mLocationCurrentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra(STATE_ID)){
                String  state = intent.getStringExtra(STATE_ID);
                if(myNotification != null){
                    myNotification.buildNotification(state);
                }

            }
        }
    };
    @Override
    public boolean stopService(Intent name) {

        return super.stopService(name);
    }
    @Override
    public void onDestroy() {
        if(myNotification != null){
            myNotification.cancel();
        }
       // mOrientationManager.restoreView();
       unregisterReceiver(mLocationCurrentReceiver);
      //  Toast.makeText(this, "The Application has been stopped!", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }



}
