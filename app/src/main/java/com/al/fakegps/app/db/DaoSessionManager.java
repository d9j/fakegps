package com.al.fakegps.app.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.mydao.gen.schema.DaoMaster;
import com.mydao.gen.schema.DaoSession;


public class DaoSessionManager {

    public static volatile DaoSession daoSession;

    private final static String DB_NAME = "gps_location_db";

    public static synchronized DaoSession newInstance(Context context){
        if(daoSession == null){
            DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context,DB_NAME,null);
            SQLiteDatabase db = devOpenHelper.getWritableDatabase();

            DaoMaster daoMaster = new DaoMaster(db);
            daoSession = daoMaster.newSession();
        }
        return  daoSession;
    }
    public static  void   close(){
        daoSession.getDatabase().close();
    }
}
