package com.al.fakegps.app;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;
import com.mydao.gen.schema.Location;

import java.util.List;

public class LocationHistoryListAdapter extends ArrayAdapter<Location>{

    private List<Location> mLocationHistoryList;
    private static class ViewHolder{
        public long id;
        public String friendlyAddress;
        public LatLng loating;
        public ViewHolder(long id, String friendlyAddress, LatLng loating) {
            this.id = id;
            this.friendlyAddress = friendlyAddress;
            this.loating = loating;
        }
    }
    public LocationHistoryListAdapter(Context context, List<Location> objects) {
        super(context, R.layout.history_item, objects);
        mLocationHistoryList = objects;
    }
    @Override
    public int getCount() {
        return mLocationHistoryList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if(rowView == null){
            LayoutInflater layoutInflater = ((Activity)getContext()).getLayoutInflater();
            rowView  = layoutInflater.inflate(R.layout.history_item,null);
            Location historyItem = mLocationHistoryList.get(position);
            String addrName = historyItem.getAltName();
            LatLng latLng = new LatLng(historyItem.getLatitude(),historyItem.getLongitude());
            if(addrName == null || addrName.isEmpty()){
                addrName = historyItem.getLocName();
            }
            ViewHolder viewHolder = new ViewHolder(historyItem.getId(),addrName,latLng);
            rowView.setTag(viewHolder);
        }
        ViewHolder viewHolder = (ViewHolder)rowView.getTag();

        TextView addressTxt =(TextView) rowView.findViewById(R.id.itemAddress);
        TextView GeoLocTxt = (TextView) rowView.findViewById(R.id.itemGeoLocation);
        addressTxt.setText(viewHolder.friendlyAddress);
        GeoLocTxt.setText(viewHolder.loating.toString());
        return rowView;
    }
    public void remove(Location Location){
        mLocationHistoryList.remove(Location);
        notifyDataSetChanged();
    }

}
