package com.al.fakegps.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.al.fakegps.app.db.LocationRepository;
import com.google.android.gms.maps.model.LatLng;
import com.mydao.gen.schema.Location;

public final class FavoriteLocationDialog extends DialogFragment {

    private LocationHistoryListAdapter mLocHisAdapter;
    private TextView historyNote;
    private LocationRepository historyRepository;
    private DialogsInterface mDialogInterface;
    private ListView mListView;
    private View parentView;
    private AlertDialog favButtonDialog;
    public static final int GROUP_ID = 8888;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(favButtonDialog != null){
            favButtonDialog.dismiss();
        }
        super.onDismiss(dialog);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        historyRepository = new LocationRepository(getActivity());
         View parentView = getActivity().getLayoutInflater().inflate(R.layout.fav_list, null);
        historyNote = (TextView) parentView.findViewById(R.id.noNotes);
        mListView = (ListView) parentView.findViewById(R.id.list);
        final Button addToFav = (Button) parentView.findViewById(R.id.addToFavButton);
        mLocHisAdapter = new LocationHistoryListAdapter(getActivity(), historyRepository.getAllFav());
        showEmptyList();
        mListView.setAdapter(mLocHisAdapter);
        mListView.setOnItemClickListener(itemClickListener);
        mListView.setOnCreateContextMenuListener(this);
        addToFav.setOnClickListener(addToFavListener);
        builder.setView(parentView);
        registerForContextMenu(mListView);
        builder.setTitle(getString(R.string.favorite_locations));
        builder.setNegativeButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }
    private View.OnClickListener addToFavListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View view = getActivity().getLayoutInflater().inflate(R.layout.fav_name,null);
            final   EditText editText = (EditText)view.findViewById(R.id.favLocationInput);
            builder.setTitle(getString(R.string.set_loc_name));
            builder.setView(view);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mDialogInterface.addToFavoritesListener(editText.getText().toString());
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            favButtonDialog =  builder.create();
            builder.show();
        }
    };
    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Location Location = mLocHisAdapter.getItem(position);
            final LatLng latLng = new LatLng(Location.getLatitude(), Location.getLongitude());
            if (mDialogInterface != null) {
                mDialogInterface.setCurrentLocation(latLng);
            }
        }
    };
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDialogInterface = (DialogsInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(GROUP_ID, R.id.renameItem, 0, R.string.rename_item);
        menu.add(GROUP_ID, R.id.deleteItem, 1, R.string.delete_item);

        MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onContextItemSelected(item);
                return true;
            }
        };
        for (int i = 0, n = menu.size(); i < n; i++)
            menu.getItem(i).setOnMenuItemClickListener(listener);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getGroupId() == GROUP_ID){
            return onContextItemSelected(item);
        }else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void  refreshList(){
        mLocHisAdapter.clear();
        mLocHisAdapter.notifyDataSetChanged();
        mLocHisAdapter = new LocationHistoryListAdapter(getActivity(),
                historyRepository.getAllFav());
        mLocHisAdapter.notifyDataSetChanged();
        mListView.setAdapter(mLocHisAdapter);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo ) item.getMenuInfo();
        if(item.getGroupId() != GROUP_ID)
            return true;
        switch (item.getItemId()){
            case R.id.deleteItem: {
                Location Location = mLocHisAdapter.getItem((int)info.id);
                if(Location == null)
                    return true;
                if(mDialogInterface != null){
                    historyRepository.deleteById(Location.getId());
                    refreshList();
                }
                showEmptyList();
                return true;
            }
            case  R.id.renameItem:{
               final Location location = mLocHisAdapter.getItem((int)info.id);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                View view = getActivity().getLayoutInflater().inflate(R.layout.fav_name,null);
                final   EditText editText = (EditText)view.findViewById(R.id.favLocationInput);
                final  String orgin = location.getAltName();
                editText.setText(orgin, null);
                builder.setTitle(getString(R.string.set_loc_name));
                builder.setView(view);

                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String newNote = editText.getText().toString();
                        if (!newNote.equals(orgin)) {
                            location.setAltName(editText.getText().toString());
                            historyRepository.renameFav(location);
                            refreshList();
                        }
                    }
                });
                favButtonDialog =  builder.create();
                builder.show();
                return true;
            }
            default:
                return super.onContextItemSelected(item);
        }

    }
    private void showEmptyList(){
        if(mLocHisAdapter.getCount() == 0){
            historyNote.setVisibility(View.VISIBLE);
        }else {
            historyNote.setVisibility(View.GONE);
        }
    }


}


