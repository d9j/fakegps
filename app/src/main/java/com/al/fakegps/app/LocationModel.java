package com.al.fakegps.app;


import android.location.Address;
import com.google.android.gms.maps.model.LatLng;

public class LocationModel{

   private String locality;

    private String country;

    private LatLng latLng;

    private float zoom = 13;

    private Address mAddress;

    private String friendlyAddress;

    public boolean fromInput = false;

    public LocationModel(LatLng latLng, float zoom) {
        this.latLng = latLng;
        this.zoom = zoom;
    }
    public LocationModel(Address address, float zoom) {
        this.zoom = zoom;
        prepareAddress(address);

    }
    public  String toString(){
        return  String.format("%s%s",
                (country != null) ? country : "",
                (locality != null) ?  "," + locality : "");
    }

    private void prepareAddress(Address address){
        mAddress = address;
        locality = address.getLocality();
        if(locality == null || locality.isEmpty()){
            if(address.getAddressLine(0) != null){
                locality = address.getAddressLine(0);
            }
        }
        country = address.getCountryName();
        if(country == null || country.isEmpty()){
            if(address.getAddressLine(1) != null){
                country = address.getAddressLine(1);
            }
        }
        latLng = new LatLng(address.getLatitude(),address.getLongitude());
        StringBuffer stringBuffer = new StringBuffer();
        int index = address.getMaxAddressLineIndex();
        int index_num = address.getMaxAddressLineIndex() > 4 ?  4 : index;
        if(index_num != -1){
            for(int i = 0; i <= index_num; i++){
                if(i == index_num){
                    stringBuffer.append(address.getAddressLine(i));
                }else {
                    stringBuffer.append(address.getAddressLine(i) + ",");
                }
            }
        }
        friendlyAddress = stringBuffer.toString();

    }
    public String getLocality() {
        return locality;
    }

    public String getCountry() {
        return country;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public Address getddress() {
        return mAddress;
    }

    public String getFriendlyAddress() {
        return friendlyAddress;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }
}


