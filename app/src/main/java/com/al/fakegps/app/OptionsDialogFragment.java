package com.al.fakegps.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class OptionsDialogFragment extends DialogFragment {


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View parentView = getActivity().getLayoutInflater().inflate(R.layout.option_layout,null);

        CheckBox bootOnStartCheck = (CheckBox) parentView.findViewById(R.id.bootOnStart);
        CheckBox keepHistory = (CheckBox) parentView.findViewById(R.id.keepHistory);
        CheckBox showIcon = (CheckBox) parentView.findViewById(R.id.showIcon);

        bootOnStartCheck.setChecked(AppSettings.bootStartup(getActivity().getApplicationContext()));
        keepHistory.setChecked(AppSettings.keepHistory(getActivity().getApplicationContext()));
        showIcon.setChecked(AppSettings.showIcon(getActivity().getApplicationContext()));

        bootOnStartCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppSettings.setStartup(getActivity().getApplicationContext(), isChecked);
            }
        });
        keepHistory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppSettings.setKeepHistory(getActivity().getApplicationContext(), b);
            }
        });

        showIcon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AppSettings.setShowIcon(getActivity().getApplicationContext(), b);
            }
        });

        builder.setView(parentView);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return  builder.create();
    }



}
