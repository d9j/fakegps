package com.al.fakegps.app;

import com.google.android.gms.maps.model.LatLng;

public interface DialogsInterface {

    void clearHistoryLocation();

    void setCurrentLocation(LatLng latLng);

   // void addToFavorites();

    void searchDialogPositiveClick(String searchInput);

    void  onFavoriteAdded();

    void onSearchDialogPositiveClick(String searchInput);

    void  addToFavoritesListener(String name);

    //void removeLocation(long id);






}
