package com.al.fakegps.app;


import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.maps.model.LatLng;

public class AppSettings {
    public final static String APP_SETTINGS = "gps_app_settings";
    public static void setCurrentLocation(Context context,LocationModel addressModel){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("latitude",String.valueOf(addressModel.getLatLng().latitude));
        editor.putString("longitude",String.valueOf(addressModel.getLatLng().longitude));
        editor.putFloat("zoom",addressModel.getZoom());
        editor.apply();
    }
    public static LocationModel getCurrentLocation(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        String latitude = sharedPreferences.getString("latitude", String.valueOf(-33.867));
        String longitude = sharedPreferences.getString("longitude", String.valueOf(151.206));
        float zoom =  sharedPreferences.getFloat("zoom", 5f);
        LatLng latLng = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));
        return  new LocationModel(latLng,zoom);
    }

    public static boolean bootStartup(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
       return sharedPreferences.getBoolean("boot_startup", false);
    }

    public static void setStartup(Context context,boolean startup){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("boot_startup",startup).apply();
    }
    public static boolean keepHistory(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("keep_history", true);
    }
    public static void setKeepHistory(Context context, boolean keepHistory){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("keep_history",keepHistory).apply();
    }

    public static boolean showIcon(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("show_icon", true);
    }
    public static void setShowIcon(Context context, boolean showIcon){
        SharedPreferences sharedPreferences = context.getSharedPreferences(APP_SETTINGS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("show_icon",showIcon).apply();
    }

}
