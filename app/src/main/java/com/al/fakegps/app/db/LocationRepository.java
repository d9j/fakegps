package com.al.fakegps.app.db;


import android.content.Context;
import com.al.fakegps.app.LocationModel;

import com.google.android.gms.maps.model.LatLng;
import com.mydao.gen.schema.LocationDao;
import com.mydao.gen.schema.Location;
import de.greenrobot.dao.query.DeleteQuery;

import java.util.*;

public class LocationRepository {

    private LocationDao locationDao;
    public LocationRepository(Context context){
        locationDao = DaoSessionManager.newInstance(context).getLocationDao();
    }
    public long addNewLocation(LocationModel addressModel){
        LatLng latLng = addressModel.getLatLng();
        String address = addressModel.getFriendlyAddress();
        Location location = new Location(null,false,
                address,
                null,
                latLng.latitude,latLng.longitude,
                addressModel.getZoom());
      return  locationDao.insert(location);
    }
    public void addNewFav(LocationModel addressModel, String favName){
        if(favName == null || favName.isEmpty()){
            favName = addressModel.getFriendlyAddress();
        }
        LatLng latLng = addressModel.getLatLng();
        Location location = new Location(null,true,
                addressModel.getFriendlyAddress(),
                favName,
                latLng.latitude,
                latLng.longitude,addressModel.getZoom());
        locationDao.insertOrReplace(location);
    }
    public List<Location> getAllLocations(){
        List<Location> locations = locationDao.queryBuilder()
                .where(LocationDao.Properties.Favorite.eq(false))
                .orderDesc(LocationDao.Properties.Id)
                .build().list();
       return locations;
    }
    public List<Location> getAllFav(){
        List<Location> favorites = locationDao.queryBuilder()
                .where(LocationDao.Properties.Favorite.eq(true))
                .build().list();
       return favorites;
    }
    public void deleteById(long id){
        locationDao.deleteByKey(id);
    }
    public void clearLocations(){
        locationDao.queryBuilder().where(LocationDao.Properties.Favorite.eq(false)).buildDelete().executeDeleteWithoutDetachingEntities();
    }
    public void clearFavorites(){
        locationDao.queryBuilder().where(LocationDao.Properties.Favorite.eq(true)).buildDelete().executeDeleteWithoutDetachingEntities();
    }
    public void renameFav(Location locationItem){
        locationDao.update(locationItem);
    }

}
