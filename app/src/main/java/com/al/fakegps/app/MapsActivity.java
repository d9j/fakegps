package com.al.fakegps.app;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.al.fakegps.app.db.DaoSessionManager;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        MapManager.MapUpdateListener,
        DialogsInterface{
   // private SupportMapFragment mMapFragment;// Might be null if Google Play services APK is not available.
    private MapManager mapManager;
    private RelativeLayout markerButton;
    private Button setLocationButton,stopAppButton;
    private RelativeLayout searchAreaButton, favoritesButton, historyButton, settingsButton;
    private LocationHistoryDialog locationHistoryDialog;
    private FavoriteLocationDialog favoriteLocationDialog;
    private OptionsDialogFragment optionsDialogFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        markerButton = (RelativeLayout)findViewById(R.id.markerButton);
        // starting up my maps service in a background

        initFragment();
    }
    private void initFragment(){
        SupportMapFragment  mMapFragment = SupportMapFragment.
                newInstance(new GoogleMapOptions().zOrderOnTop(false)
                        .zoomControlsEnabled(true)
                        .zoomGesturesEnabled(true)
                        );
        FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map, mMapFragment);
        fragmentTransaction.commit();
        //   markerButton.setBackground();
        mMapFragment.getMapAsync(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapManager = new MapManager(MapsActivity.this,googleMap);
        setLocationButton = (Button)findViewById(R.id.setLocationButton);
        stopAppButton = (Button)findViewById(R.id.stopAppButton);
        searchAreaButton = (RelativeLayout) findViewById(R.id.searchAreaButton);
        favoritesButton = (RelativeLayout) findViewById(R.id.favoritesButton);
        historyButton = (RelativeLayout) findViewById(R.id.historyButton);
        settingsButton = (RelativeLayout) findViewById(R.id.settingsButton);
        setLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapManager.setLocation();
            }
        });

        stopAppButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapManager.stopFakeLocation();
                stopService(new Intent(MapsActivity.this, MyMapService.class));
                Toast.makeText(MapsActivity.this,getString(R.string.application_stopped), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        searchAreaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialog = new SearchDialogFragment();
                dialog.show(getSupportFragmentManager(), "SearchDialogFragment");
            }
        });
        favoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteLocationDialog = new FavoriteLocationDialog();
                favoriteLocationDialog.show(getSupportFragmentManager(),"FavoriteLocationDialog");
            }
        });
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationHistoryDialog = new LocationHistoryDialog();
                locationHistoryDialog.show(getSupportFragmentManager(),"LocationHistoryDialog");
                //   historyListDialog.getListView();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionsDialogFragment = new OptionsDialogFragment();
                optionsDialogFragment.show(getSupportFragmentManager(),"OptionsDialogFragment");
            }
        });
    }
    @Override
    public void onMapUpdate(final  String address) {
        synchronized (this){
                    Intent i = new Intent(MyMapService.STATE_RECEIVER);
                    i.putExtra(MyMapService.STATE_ID,address);
                    sendBroadcast(i);
                    finish();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();


    }
    @Override
    public void clearHistoryLocation() {
        mapManager.clearHistory();
    }
    @Override
    public void setCurrentLocation(LatLng latLng) {
        mapManager.setLocation(latLng, true);
        dismissOpenDialogs();
    }
    private void dismissOpenDialogs(){
        if(locationHistoryDialog != null){
            locationHistoryDialog.dismiss();
        }
        if(favoriteLocationDialog != null){
            favoriteLocationDialog.dismiss();
        }
        if(optionsDialogFragment != null){
            optionsDialogFragment.dismiss();
        }
    }
    @Override
    public void addToFavoritesListener(String name) {
        mapManager.setLocation(name,true);
        dismissOpenDialogs();
    }
    @Override
    public void onFavoriteAdded() {
        dismissOpenDialogs();
    }

    @Override
    public void searchDialogPositiveClick(String searchInput) {
        mapManager.setLocation(searchInput);
    }

    @Override
    public void onSearchDialogPositiveClick(String searchInput) {

    }
}