package com.al.fakegps.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.ContextMenu;

import android.view.MenuItem;
import android.view.View;

import android.widget.*;
import com.al.fakegps.app.db.LocationRepository;
import com.google.android.gms.maps.model.LatLng;
import com.mydao.gen.schema.Location;

public final class LocationHistoryDialog extends DialogFragment {

    private LocationHistoryListAdapter mLocHisAdapter;
    private TextView historyNote;
    private LocationRepository historyRepository;
    private DialogsInterface mDialogInterface;
    private ListView mListView;
    public static final int GROUP_ID = 9999;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        historyRepository = new LocationRepository(getActivity());
        final View parent_view = getActivity().getLayoutInflater().inflate(R.layout.history_list, null);
        historyNote = (TextView) parent_view.findViewById(R.id.noHistoryNote);
        mListView = (ListView) parent_view.findViewById(R.id.list);
        final Button clearHistory = (Button) parent_view.findViewById(R.id.clearHistory);
        mLocHisAdapter = new LocationHistoryListAdapter(getActivity(), historyRepository.getAllLocations());
        showEmptyList();
        mListView.setAdapter(mLocHisAdapter);
        mListView.setOnItemClickListener(itemClickListener);
        mListView.setOnCreateContextMenuListener(this);
        clearHistory.setOnClickListener(clearHistoryListener);
        builder.setView(parent_view);
        registerForContextMenu(mListView);
        builder.setTitle(getString(R.string.location_history));
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }
    private View.OnClickListener clearHistoryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mDialogInterface != null) {
                mDialogInterface.clearHistoryLocation();
                mLocHisAdapter.clear();
                mLocHisAdapter.notifyDataSetChanged();
                showEmptyList();
            }
        }
    };
    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Location Location = mLocHisAdapter.getItem(position);
            final LatLng latLng = new LatLng(Location.getLatitude(), Location.getLongitude());
            if (mDialogInterface != null) {
                mDialogInterface.setCurrentLocation(latLng);
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mDialogInterface = (DialogsInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(GROUP_ID, R.id.deleteItem, 0, R.string.delete_item);
        MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onContextItemSelected(item);
                return true;
            }
        };
        for (int i = 0, n = menu.size(); i < n; i++)
            menu.getItem(i).setOnMenuItemClickListener(listener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getGroupId() == GROUP_ID){
            return onContextItemSelected(item);
        }else {
            return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo ) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.deleteItem: {
                Log.d("item_delete", "Delete:  " + info.id);
               Location Location = mLocHisAdapter.getItem((int)info.id);
                if(Location == null)
                    return true;
                if(mDialogInterface != null){
                    historyRepository.deleteById(Location.getId());
                    mLocHisAdapter.clear();
                    mLocHisAdapter.notifyDataSetChanged();
                    mLocHisAdapter = new LocationHistoryListAdapter(getActivity(),
                            historyRepository.getAllLocations());
                    mLocHisAdapter.notifyDataSetChanged();
                    mListView.setAdapter(mLocHisAdapter);
                }
                showEmptyList();
                return true;
            }
            default:
                return super.onContextItemSelected(item);
        }
    }
    private void showEmptyList(){
        if(mLocHisAdapter.getCount() == 0){
            historyNote.setVisibility(View.VISIBLE);
        }else {
            historyNote.setVisibility(View.GONE);
        }
    }
}


