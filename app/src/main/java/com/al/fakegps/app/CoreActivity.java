package com.al.fakegps.app;


import android.app.Application;
import com.al.fakegps.app.db.DaoSessionManager;

public class CoreActivity extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DaoSessionManager.newInstance(this);
    }
}
