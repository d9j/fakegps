package com.al.fakegps.app;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.al.fakegps.app.db.LocationRepository;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapManager {
    private GoogleMap mGoogleMap;
    private Context mContext;
    private MyLocationManager myLocationManager;
    private Marker mMarker;
    private LocationRepository locationHistoryRepo;
    private  MapUpdateListener mapUpdateListener;
    private volatile  float mMapZoom = 13;
    public MapManager( Context context,GoogleMap googleMap){
        this.mGoogleMap = googleMap;
        this.mContext = context;
        mapUpdateListener = (MapUpdateListener)context;
        this.myLocationManager = new MyLocationManager(context);
        this.locationHistoryRepo = new LocationRepository(context);
        setUpMap();
    }
    public interface MapUpdateListener{
        void  onMapUpdate(final String locationName);
    }

    private void restoreLocation(){
        LocationModel addressModel = AppSettings.getCurrentLocation(mContext);
        LatLng sydney = addressModel.getLatLng();
        MarkerOptions markerOptions = new MarkerOptions().position(sydney);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, addressModel.getZoom()));
        mMarker = mGoogleMap.addMarker(markerOptions);
        mMarker.setVisible(false);
    }
    private void setUpMap() {
        //Setting up default location.
        restoreLocation();
        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                // Get zoom specified by user
                mMapZoom = cameraPosition.zoom;
                LatLng latLng = mGoogleMap.getCameraPosition().target;
                mMarker.setPosition(latLng);
            }
        });
    }
    public void setLocation(){
        new ReverseGeocodingAsync(mMarker.getPosition(), false).execute();
    }
    public void setLocation(String input){
        new ReverseGeocodingAsync(input).execute();
    }
    public void setLocation(String name,boolean fromFavorites){
        new ReverseGeocodingAsync(fromFavorites, name, mMarker.getPosition()).execute();
    }
    public void setLocation(LatLng latLng, boolean fromHistory){
        new ReverseGeocodingAsync(latLng, fromHistory).execute();
    }
    public void clearHistory(){
        locationHistoryRepo.clearLocations();
    }
    public void stopFakeLocation(){
        // Remove Fake Provider
        myLocationManager.removeProvider();
    }
    private class ReverseGeocodingAsync extends AsyncTask<Void,LocationModel,LocationModel> {
        private LatLng mlatLng;
        private String inputedAddress;
        private String favName;
        private boolean fromHistory = false;
        private boolean fromFavorites = false;
        private boolean geocoderexec = false;
        public ReverseGeocodingAsync( LatLng latLng, boolean fromHistory){
            this.mlatLng = latLng;
            this.fromHistory = fromHistory;
        }

        public ReverseGeocodingAsync(boolean fromFavorites, String favName, LatLng latLng){
            this.fromFavorites = fromFavorites;
            this.favName = favName;
            this.mlatLng = latLng;
        }

        public ReverseGeocodingAsync( String inputedAddress){
            this.inputedAddress = inputedAddress;
        }
        @Override
        protected LocationModel doInBackground(Void... params) {
            Geocoder geocoder = new Geocoder(mContext);
            List<Address> addressList = null;
            LocationModel addressModel;
            boolean fromInput = false;
            try {
                if(mlatLng != null || fromFavorites){
                    double latitude = mlatLng.latitude;
                    double longitude = mlatLng.longitude;
                    SysUtil.showLog("setting geocoder address list");
                    addressList = geocoder.getFromLocation(latitude, longitude,1);
                }else  if(inputedAddress != null){
                    SysUtil.showLog("inputted address not empty");
                    addressList =  geocoder.getFromLocationName(inputedAddress, 1);
                    fromInput = true;
                }
            } catch (IOException e) {
                SysUtil.showLog(e);
                geocoderexec = true;
                return null;
            }
            if(addressList == null) return null;
            addressModel = parseAddress(addressList);
            if(addressModel != null){
                if(fromFavorites) {
                    locationHistoryRepo.addNewFav(addressModel,favName);
                    return addressModel;
                }
                addressModel.fromInput = fromInput;
                addressModel.setZoom(mMapZoom);
            }
          //  locationHistoryRepo.addNewFav(addressModel);
            return addressModel;
        }      @Override
       protected void onPostExecute(final LocationModel addressModel) {
            if(geocoderexec){
                geocoderUnavailable();
                return;
            }
            if(addressModel == null){
                unableFindLocation();
                return;
            }
            // exit if from favorites
            if(fromFavorites){

                return;
            }
            // Just Move Camera if from input or from history
            if(addressModel.fromInput || fromHistory){
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(addressModel.getLatLng()));
                String address = addressModel.getFriendlyAddress();
                if(address == null ){
                    address = addressModel.toString();
                }
                Toast.makeText(mContext,mContext.getString(R.string.found_location,address),Toast.LENGTH_SHORT).show();
                return;
            }
            // Set Location
            setUserLocation(addressModel);
        }
        private boolean unableFindLocation(){
            Toast.makeText(mContext, mContext.getString(R.string.unable_set_location), Toast.LENGTH_SHORT).show();
            return  cancel(true);
        }
        private LocationModel parseAddress(List<Address> addresses){
            LocationModel addressModel = null;
            Address address = addresses.size() > 0 ? addresses.get(0) : null;
            if(address != null){
                try {
                    addressModel = new LocationModel(address,mMapZoom);
                }catch (IllegalStateException e){
                    SysUtil.showLog(e);
                }
            }
            return  addressModel;
        }
        private void geocoderUnavailable(){
            AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
            alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.setCancelable(false);
            alert.setTitle(mContext.getString(R.string.geocoding_unavailable_title));
            alert.setMessage(mContext.getString(R.string.geocoding_unavailable_message));
            alert.create().show();

        }
        private void setUserLocation(final  LocationModel addressModel){
            new AsyncTask<Void,Void,LatLng>(){
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onCancelled() {
                    unableFindLocation();
                }
                @Override
                protected LatLng doInBackground(Void... voids) {
                    try {
                        myLocationManager.setLocation(addressModel.getLatLng());
                    }catch (IllegalArgumentException e){
                        SysUtil.showLog(e);
                        cancel(true);
                    }
                    // Setting up location
                    final LatLng latLng = addressModel.getLatLng();
                    AppSettings.setCurrentLocation(mContext, addressModel);
                    mContext.startService(new Intent(mContext, MyMapService.class));
                    if(AppSettings.keepHistory(mContext.getApplicationContext())){
                        locationHistoryRepo.addNewLocation(addressModel);
                    }
                    return latLng;
                }
                @Override

                protected void onPostExecute(LatLng latLng) {
                    mMarker.setPosition(latLng);
                  Toast.makeText(mContext,mContext.getString(R.string.location_set,addressModel.getFriendlyAddress()),Toast.LENGTH_LONG).show();
                    if(mapUpdateListener != null){
                        mapUpdateListener.onMapUpdate(latLng.toString());
                    }
                }
            }.execute();
        }

    }

    public LocationRepository getLocationHistoryRepo() {
        return locationHistoryRepo;
    }
}
