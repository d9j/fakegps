package com.al.fakegps.app;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;


public class SysUtil {
    public final static String APP_LOG = "fake_gps_log";
    public static Date getDbDate(){
        long yourmilliseconds = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        Date resultdate = new Date(yourmilliseconds);
        return  resultdate;
    }
    public static void showLog(Exception e){
        Log.d(APP_LOG, "====== Start Exception======" +
                e.getMessage() + "\n"  + e.fillInStackTrace() + "\n" +
                        "====== End Exception======"
        );
    }

    public static void showLog(String msg){
        Log.d(APP_LOG, msg);
    }
}
