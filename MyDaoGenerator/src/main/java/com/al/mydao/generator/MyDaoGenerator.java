package com.al.mydao.generator;
import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;
public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(11, "com.mydao.gen.schema");
        addLocation(schema);
        new DaoGenerator().generateAll(schema, args[0]);
    }

    private static void addLocation(Schema schema){
        Entity location= schema.addEntity("Location");
        location.addIdProperty().autoincrement();
        location.addBooleanProperty("favorite").getProperty();
        location.addStringProperty("locName");
        location.addStringProperty("altName");
        location.addDoubleProperty("latitude");
        location.addDoubleProperty("longitude");
        location.addFloatProperty("zoom");

    }
}
